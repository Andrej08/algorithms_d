/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module d_frame_management;

/**
    This straight D port assumes that the linked-list has
    already allocated some frames. The code to do this
    is in an upcoming chapter (after 5.4).
*/

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;

import d_singly_linked_list;

/**
    Allocate a frame by taking a free one from the linked list,
    and return its index.
*/
size_t alloc_frame(LinkedList!size_t frames)
{
    enforce(frames.size > 0);

    size_t* data;
    frames.remove_front(&data);

    size_t frame_number = *data;
    free(data);

    return frame_number;
}

/** Free a frame from the linked list. */
void free_frame(LinkedList!size_t frames, size_t frame_number)
{
    size_t* data = new size_t;
    *data = frame_number;
    frames.insert_front(data);
}

unittest
{
}
