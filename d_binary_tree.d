/*
 *            Copyright Andrej Mitrovic 2013.
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 */
module d_binary_tree;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

class Tree(T)
{
    static struct Node
    {
        T data;
        Node* left;
        Node* right;
    }

    void destroy()
    {
        remLeft(null);
    }

    int insertLeft(Node* node, T data)
    {
        BiTreeNode* new_node;
        BiTreeNode** position;

        // Determine where to insert the node.
        if (node is null)
        {
            // Allow insertion at the root only in an empty tree.
            if (bitree_size(tree) > 0)
                return -1;

            position = &tree.root;
        }
        else
        {
            // Normally allow insertion only at the end of a branch.
            if (bitree_left(node) !is null)
                return -1;

            position = &node.left;
        }

        // Allocate storage for the node.
        if ((new_node = cast(BiTreeNode*)malloc(BiTreeNode.sizeof)) is null)
            return -1;

        // Insert the node into the tree.
        new_node.data  = cast(void*)data;
        new_node.left  = null;
        new_node.right = null;
        *position      = new_node;

        // Adjust the size of the tree to account for the inserted node.
        tree.size++;

        return 0;
    }

    Node* root;
    size_t size;  // tree size
}

int bitree_ins_left(BiTree* tree, BiTreeNode* node, void* data)
{
    BiTreeNode* new_node;
    BiTreeNode** position;

    // Determine where to insert the node.
    if (node is null)
    {
        // Allow insertion at the root only in an empty tree.
        if (bitree_size(tree) > 0)
            return -1;

        position = &tree.root;
    }
    else
    {
        // Normally allow insertion only at the end of a branch.
        if (bitree_left(node) !is null)
            return -1;

        position = &node.left;
    }

    // Allocate storage for the node.
    if ((new_node = cast(BiTreeNode*)malloc(BiTreeNode.sizeof)) is null)
        return -1;

    // Insert the node into the tree.
    new_node.data  = cast(void*)data;
    new_node.left  = null;
    new_node.right = null;
    *position      = new_node;

    // Adjust the size of the tree to account for the inserted node.
    tree.size++;

    return 0;
}

//~ // --------------------------- bitree_ins_right ---------------------------

//~ int bitree_ins_right(BiTree* tree, BiTreeNode* node, void* data)
//~ {
    //~ BiTreeNode* new_node;
    //~ BiTreeNode** position;

    //~ // Determine where to insert the node.
    //~ if (node is null)
    //~ {
        //~ // Allow insertion at the root only in an empty tree.
        //~ if (bitree_size(tree) > 0)
            //~ return -1;

        //~ position = &tree.root;
    //~ }
    //~ else
    //~ {
        //~ // Normally allow insertion only at the end of a branch.
        //~ if (bitree_right(node) !is null)
            //~ return -1;

        //~ position = &node.right;
    //~ }

    //~ // Allocate storage for the node.
    //~ if ((new_node = cast(BiTreeNode*)malloc(BiTreeNode.sizeof)) is null)
        //~ return -1;

    //~ // Insert the node into the tree.
    //~ new_node.data  = cast(void*)data;
    //~ new_node.left  = null;
    //~ new_node.right = null;
    //~ *position      = new_node;

    //~ // Adjust the size of the tree to account for the inserted node.
    //~ tree.size++;

    //~ return 0;
//~ }

//~ void bitree_rem_left(BiTree* tree, BiTreeNode* node)
//~ {
    //~ BiTreeNode** position;

    //~ // Do not allow removal from an empty tree.
    //~ if (bitree_size(tree) == 0)
        //~ return;

    //~ // Determine where to remove nodes.
    //~ if (node is null)
        //~ position = &tree.root;
    //~ else
        //~ position = &node.left;

    //~ // Remove the nodes.
    //~ if (*position !is null)
    //~ {
        //~ bitree_rem_left(tree, *position);
        //~ bitree_rem_right(tree, *position);

        //~ if (tree.destroy !is null)
        //~ {
            //~ // Call a user-defined function to free dynamically allocated data.
            //~ tree.destroy((*position).data);
        //~ }

        //~ free(*position);
        //~ *position = null;

        //~ // Adjust the size of the tree to account for the removed node.
        //~ tree.size--;
    //~ }

    //~ return;
//~ }

//~ void bitree_rem_right(BiTree* tree, BiTreeNode* node)
//~ {
    //~ BiTreeNode** position;

    //~ // Do not allow removal from an empty tree.
    //~ if (bitree_size(tree) == 0)
        //~ return;

    //~ // Determine where to remove nodes.
    //~ if (node is null)
        //~ position = &tree.root;
    //~ else
        //~ position = &node.right;

    //~ // Remove the nodes.
    //~ if (*position !is null)
    //~ {
        //~ bitree_rem_left(tree, *position);
        //~ bitree_rem_right(tree, *position);

        //~ if (tree.destroy !is null)
        //~ {
            //~ // Call a user-defined function to free dynamically allocated data.
            //~ tree.destroy((*position).data);
        //~ }

        //~ free(*position);
        //~ *position = null;

        //~ // Adjust the size of the tree to account for the removed node.
        //~ tree.size--;
    //~ }

    //~ return;
//~ }

//~ int bitree_merge(BiTree* merge, BiTree* left, BiTree* right, void* data)
//~ {
    //~ // Initialize the merged tree.
    //~ bitree_init(merge, left.destroy);

    //~ // Insert the data for the root node of the merged tree.
    //~ if (bitree_ins_left(merge, null, data) != 0)
    //~ {
        //~ bitree_destroy(merge);
        //~ return -1;
    //~ }

    //~ // Merge the two binary trees into a single binary tree.
    //~ bitree_root(merge).left  = bitree_root(left);
    //~ bitree_root(merge).right = bitree_root(right);

    //~ // Adjust the size of the new binary tree.
    //~ merge.size = merge.size + bitree_size(left) + bitree_size(right);

    //~ // Do not let the original trees access the merged nodes.
    //~ left.root  = null;
    //~ left.size  = 0;
    //~ right.root = null;
    //~ right.size = 0;

    //~ return 0;
//~ }
