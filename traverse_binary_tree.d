/*
 *            Copyright Andrej Mitrovic 2013.
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 */
module traverse_binary_tree;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

import binary_tree;
import c_singly_linked_list;

// Load the list with a preorder listing of the tree.
void preorder(BiTreeNode* node, LinkedList* list)
{
    list_ins_next(list, list_tail(list), bitree_data(node));

    if (!bitree_is_eob(bitree_left(node)))
        preorder(bitree_left(node), list);

    if (!bitree_is_eob(bitree_right(node)))
        preorder(bitree_right(node), list);
}

// Load the list with an inorder listing of the tree.
void inorder(BiTreeNode* node, LinkedList* list)
{
    if (!bitree_is_eob(bitree_left(node)))
        inorder(bitree_left(node), list);

    list_ins_next(list, list_tail(list), bitree_data(node));

    if (!bitree_is_eob(bitree_right(node)))
        inorder(bitree_right(node), list);
}

// Load the list with a postorder listing of the tree.
void postorder(BiTreeNode* node, LinkedList* list)
{
    if (!bitree_is_eob(bitree_left(node)))
        postorder(bitree_left(node), list);

    if (!bitree_is_eob(bitree_right(node)))
        postorder(bitree_right(node), list);

    list_ins_next(list, list_tail(list), bitree_data(node));
}
