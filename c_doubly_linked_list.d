/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module c_doubly_linked_list;

/**
    A straight port of the C doubly-linked list structure to D.
    A native D port is in d_doubly_linked_list.d.
*/

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;

/**
    Allocate a structure instance of type via C malloc,
    initialize it to T.init and return it.
*/
T* alloc(T)()
{
    auto result = enforce(cast(T*)malloc(T.sizeof));
    // note: malloc gives us untyped random memory, we have to initialize the memory.
    *result = T.init;
    return result;
}

/// An element of a doubly-linked list.
struct ListElement
{
    void* data;
    ListElement* prev;
    ListElement* next;
}

/// The type of the function that will destroy data after a linked-list element is removed.
alias DestroyFunction = void function(void* data);

/// The type of the function for matching each element's data.
alias MatchFunction = int function(const(void)* data1, const(void)* data2);

/// The doubly-linked list.
struct LinkedList
{
    /// The head of the list, null if list is empty.
    ListElement* head;

    /// The tail of the list, null if list is empty.
    ListElement* tail;

    /// The function that will destroy data after a linked-list element is removed.
    DestroyFunction destroy;

    /// The function for matching each element's data (unused in this module).
    MatchFunction match;

    /// The size of the list.
    size_t size;
}

/** Initialize the linked-list. $(D list) is assumed to point to valid memory. */
void list_init(LinkedList* list, DestroyFunction destroy = null)
{
    list.head = null;
    list.tail = null;
    list.size = 0;
    list.destroy = destroy;
}

/**
    Destroy the linked-list. If a non-null $(D destroy) function was provided in the
    call to $(D list_init), it will be invoked on each element of the linked-list
    after the element is removed from the list.
*/
void list_destroy(LinkedList* list)
{
    while (list.size > 0)
    {
        // remove the element from the tail, but store back the data for removal.
        void* removed_data;
        list_remove(list, list_tail(*list), &removed_data);

        // call the cleanup destroy function for the removed data.
        if (list.destroy !is null)
            list.destroy(removed_data);
    }

    // clear the structure
    memset(list, 0, LinkedList.sizeof);
}

/**
    Insert $(D data) into the linked list after $(D old_element).
    If old_element is null the list must be empty, and $(D data)
    will be added to the head of the list.

    Returns the new element.
*/
ListElement* list_ins_next(LinkedList* list, ListElement* old_element, void* data)
{
    // Do not allow a null element unless the list is empty.
    if (old_element is null)
        enforce(list_size(*list) == 0);

    auto new_element = alloc!ListElement;
    new_element.data = data;

    if (list_size(*list) == 0)  // Handle insertion when the list is empty.
    {
        list.head      = new_element;
        list.head.prev = null;
        list.head.next = null;
        list.tail      = new_element;
    }
    else  // Handle insertion when the list is not empty.
    {
        new_element.next = old_element.next;
        new_element.prev = old_element;

        if (old_element.next is null)  // this was the only element, make new element tail.
            list.tail = new_element;
        else
            old_element.next.prev = new_element;  // update next element to point back.

        // new element follows the old element.
        old_element.next = new_element;
    }

    // Adjust the size of the list to account for the inserted element.
    list.size++;

    return new_element;
}

/**
    Insert $(D data) into the linked list before $(D old_element).
    If old_element is null the list must be empty, and $(D data)
    will be added to the head of the list.

    Returns the new element.
*/
ListElement* list_ins_prev(LinkedList* list, ListElement* old_element, void* data)
{
    // Do not allow a null element unless the list is empty.
    if (old_element is null)
        enforce(list_size(*list) == 0);

    auto new_element = alloc!ListElement;
    new_element.data = data;

    if (list_size(*list) == 0)  // Handle insertion when the list is empty.
    {
        list.head      = new_element;
        list.head.prev = null;
        list.head.next = null;
        list.tail      = new_element;
    }
    else  // Handle insertion when the list is not empty.
    {
        new_element.next = old_element;
        new_element.prev = old_element.prev;

        if (old_element.prev is null)  // this was the only element, make new element head.
            list.head = new_element;
        else
            old_element.prev.next = new_element;

        // old element follows the new element.
        old_element.prev = new_element;
    }

    // Adjust the size of the list to account for the inserted element.
    list.size++;

    return new_element;
}

/**
    Remove the element that follows $(D old_element) in the linked-list.
    $(D data) is a pointer to a pointer to data which will hold the
    data pointer of the removed element upon the return of the function.
*/
void list_rem_next(LinkedList* list, ListElement* old_element, void** data)
{
    enforce(list.size != 0);  // the list must not be empty.

    ListElement* removed_element;  // store element for free() call.

    if (old_element is null)  // destroy the head element
    {
        removed_element = list.head;

        // store the old data for deletion.
        *data = removed_element.data;

        // set the new head.
        list.head = list.head.next;  // kind of like popFront().

        // check and update the tail.
        if (list.size == 1)  // the last item was removed (no more tail).
            list.tail = null;
    }
    else  // destroy the element after element
    {
        enforce(old_element.next !is null);  // there must be a next element.

        removed_element = old_element.next;

        // store the old data.
        *data = removed_element.data;

        // point to the element after the destroyed element
        old_element.next = old_element.next.next;

        // check and update the tail
        if (old_element.next is null)
            list.tail = old_element;
    }

    // release resources
    free(removed_element);

    // update the list size
    list.size--;
}

/**
    Remove the element $(D element) in the linked-list.
    $(D element) must not be null, and the linked-list must not be empty.
*/
void list_remove(LinkedList* list, ListElement* element, void** data)
{
    enforce(element !is null);
    enforce(list.size != 0);

    // store the old data for deletion.
    *data = element.data;

    if (element is list.head) // Handle removal from the head of the list.
    {
        list.head = element.next;  // update the new head since head will be removed.

        if (list.head is null)
            list.tail = null;  // if this was the last element, then both the head and tail should be null.
        else
            element.next.prev = null;  // new head means prev points to null.
    }
    else  // Non-head of the list.
    {
        element.prev.next = element.next;

        if (element.next is null)  // this was the tail
            list.tail = element.prev;
        else
            element.next.prev = element.prev;
    }

    // release resources
    free(element);

    // update the list size
    list.size--;
}

///
size_t list_size(LinkedList list) { return list.size; }

///
ListElement* list_head(LinkedList list) { return list.head; }

///
ListElement* list_tail(LinkedList list) { return list.tail; }

///
bool list_is_head(LinkedList list, ListElement* element) { return list.head is element; }

///
bool list_is_tail(LinkedList list, ListElement* element) { return list.tail is element; }

///
void* list_data(ListElement* element) { return element.data; }

///
ListElement* list_next(ListElement* element) { return element.next; }

///
unittest
{
    auto list = alloc!LinkedList();
    list_init(list);
    assert(list.size == 0);

    char* x = new char;
    *x = 'x';

    // add x to the head of the list (x)
    auto element = list_ins_next(list, null, x);
    assert(list.size == 1);

    char* y = new char;
    *y = 'y';

    // add y to the head of the list (y, x)
    list_ins_prev(list, element, y);
    assert(list.size == 2);

    char* z = new char;
    *z = 'z';

    // add z after head (y, z, x)
    list_ins_next(list, list.head, z);
    assert(list.size == 3);

    {
    size_t idx;
    auto node = list.head;
    while (node !is null)
    {
        switch (idx)
        {
            case 0: assert(node.data is y); break;
            case 1: assert(node.data is z); break;
            case 2: assert(node.data is x); break;
            default: assert(0);
        }

        writefln("Node: %s", *cast(char*)node.data);
        node = node.next;
        idx++;
    }
    }

    {
    // attempt reverse
    size_t idx;
    auto node = list.tail;
    while (node !is null)
    {
        switch (idx)
        {
            case 0: assert(node.data is x); break;
            case 1: assert(node.data is z); break;
            case 2: assert(node.data is y); break;
            default: assert(0);
        }

        writefln("Node: %s", *cast(char*)node.data);
        node = node.prev;
        idx++;
    }
    }

    list_destroy(list);
    assert(list.size == 0);
}
