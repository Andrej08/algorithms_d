/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module page;

import c_circular_singly_linked_list;

struct Page
{
    int number;
    int reference;
}

int replace_page(ListElement** current)
{
    // Circle through the list of pages until one is found to replace.
    while ((cast(Page*)(*current).data).reference != 0)
    {
        (cast(Page*)(*current).data).reference = 0;
        *current = list_next(*current);
    }

    return (cast(Page*)(*current).data).number;
}
