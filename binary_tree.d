/*
 *            Copyright Andrej Mitrovic 2013.
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 */
module binary_tree;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

struct TreeNode
{
    void* data;
    TreeNode* left;
    TreeNode* right;
}

// Define a structure for binary trees.
alias CompareFunc = int function(void* key1, void* key2);
extern(C) alias DestroyFunc = void function(void* data);

struct Tree
{
    int size;

    CompareFunc compare;
    DestroyFunc destroy;
    TreeNode* root;
}

void bitree_init(Tree* tree, DestroyFunc destroy)
{
    tree.size    = 0;
    tree.compare = null;
    tree.destroy = destroy;
    tree.root    = null;
}

void bitree_destroy(Tree* tree)
{
    // Remove all the nodes from the tree.
    bitree_rem_left(tree, null);

    // No operations are allowed now, but clear the structure as a precaution.
    memset(tree, 0, Tree.sizeof);
}

int bitree_ins_left(Tree* tree, TreeNode* node, void* data)
{
    TreeNode* new_node;
    TreeNode** position;

    /* Determine where to insert the node. */

    if (node is null)
    {
        // Allow insertion at the root only in an empty tree.
        if (tree.size > 0)
            return -1;

        position = &tree.root;
    }
    else
    {
        // Normally allow insertion only at the end of a branch.
        if (node.left !is null)
            return -1;

        position = &node.left;
    }

    // Allocate storage for the node.
    if ((new_node = cast(TreeNode*)malloc(TreeNode.sizeof)) is null)
        return -1;

    // Insert the node into the tree.
    new_node.data  = cast(void*)data;
    new_node.left  = null;
    new_node.right = null;
    *position      = new_node;

    // Adjust the size of the tree to account for the inserted node.
    tree.size++;

    return 0;
}

int bitree_ins_right(Tree* tree, TreeNode* node, void* data)
{
    TreeNode* new_node;
    TreeNode** position;

    // Determine where to insert the node.
    if (node is null)
    {
        // Allow insertion at the root only in an empty tree.
        if (tree.size > 0)
            return -1;

        position = &tree.root;
    }
    else
    {
        // Normally allow insertion only at the end of a branch.
        if (node.right !is null)
            return -1;

        position = &node.right;
    }

    // Allocate storage for the node.
    if ((new_node = cast(TreeNode*)malloc(TreeNode.sizeof)) is null)
        return -1;

    // Insert the node into the tree.
    new_node.data  = cast(void*)data;
    new_node.left  = null;
    new_node.right = null;
    *position      = new_node;

    // Adjust the size of the tree to account for the inserted node.
    tree.size++;

    return 0;
}

void bitree_rem_left(Tree* tree, TreeNode* node)
{
    TreeNode** position;

    // Do not allow removal from an empty tree.
    if (tree.size == 0)
        return;

    // Determine where to remove nodes.
    if (node is null)
        position = &tree.root;
    else
        position = &node.left;

    // Remove the nodes.
    if (*position !is null)
    {
        // remove entire tree recursively
        bitree_rem_left(tree, *position);
        bitree_rem_right(tree, *position);

        if (tree.destroy !is null)
        {
            // Call a user-defined function to free dynamically allocated data.
            tree.destroy((*position).data);
        }

        free(*position);
        *position = null;

        // Adjust the size of the tree to account for the removed node.
        tree.size--;
    }

    return;
}

void bitree_rem_right(Tree* tree, TreeNode* node)
{
    TreeNode** position;

    // Do not allow removal from an empty tree.
    if (tree.size == 0)
        return;

    // Determine where to remove nodes.
    if (node is null)
        position = &tree.root;
    else
        position = &node.right;

    // Remove the nodes.
    if (*position !is null)
    {
        bitree_rem_left(tree, *position);
        bitree_rem_right(tree, *position);

        if (tree.destroy !is null)
        {
            // Call a user-defined function to free dynamically allocated data.
            tree.destroy((*position).data);
        }

        free(*position);
        *position = null;

        // Adjust the size of the tree to account for the removed node.
        tree.size--;
    }

    return;
}

/// Merge the left and right trees, and associate data for the new root node
int bitree_merge(Tree* merge, Tree* left, Tree* right, void* data)
{
    // Initialize the merged tree.
    bitree_init(merge, left.destroy);  // left or right, doesn't matter

    // Insert a root node for the merge tree and the data
    if (bitree_ins_left(merge, null, data) != 0)
    {
        bitree_destroy(merge);
        return -1;
    }

    // Merge the two binary trees into a single binary tree.
    merge.root.left  = left.root;
    merge.root.right = right.root;

    // Adjust the size of the new binary tree.
    // 1 (root node) + left.sizeof + right.sizeof
    merge.size = merge.size + left.size + right.size;

    // Zero out the merged trees.
    left.root  = null;
    left.size  = 0;
    right.root = null;
    right.size = 0;

    return 0;
}

void print_preorder(TreeNode* node)
{
    if (node !is null)
    {
        writef("Node = %03d\n", *cast(int*)node.data);

        if (node.left !is null)
            print_preorder(node.left);

        if (node.right !is null)
            print_preorder(node.right);
    }
}

void print_inorder(TreeNode* node)
{
    if (node !is null)
    {
        if (node.left !is null)
            print_inorder(node.left);

        writef("Node = %03d\n", *cast(int*)node.data);

        if (node.right !is null)
            print_inorder(node.right);
    }
}

void print_postorder(TreeNode* node)
{
    if (node !is null)
    {
        if (node.left !is null)
            print_postorder(node.left);

        if (node.right !is null)
            print_postorder(node.right);

        writef("Node = %03d\n", *cast(int*)node.data);
    }
}

import queue;

void print_levelorder(TreeNode* root)
{
    Queue queue;
    queue_init(&queue, &free);

    queue_enqueue(&queue, root);
    while (queue.size > 0)
    {
        TreeNode* node;
        queue_dequeue(&queue, cast(void**)&node);
        writefln("Node = %03d", *cast(int*)node.data);

        if (node.left !is null)
            queue_enqueue(&queue, node.left);

        if (node.right !is null)
            queue_enqueue(&queue, node.right);

    }
}

int insert_int(Tree* tree, int item)
{
    // where to insert
    enum Direction
    {
        none,  // sentinel
        head,
        left,
        right,
    }

    Direction direction = Direction.head;
    TreeNode* node = tree.root;
    TreeNode* parent;

    // Insert item assuming a binary tree organized as a binary search tree.
    while (node !is null)
    {
        parent = node;

        if (item == *cast(int*)node.data)
        {
            return -1;  // item already in tree
        }
        else
        if (item < *cast(int*)node.data)
        {
            node      = node.left;
            direction = Direction.left;
        }
        else
        {
            node      = node.right;
            direction = Direction.right;
        }
    }

    int* data;
    if ((data = cast(int*)malloc(int.sizeof)) is null)
        return -1;

    *data = item;

    if (direction == Direction.head)
        return bitree_ins_left(tree, null, data);

    if (direction == Direction.left)
        return bitree_ins_left(tree, parent, data);

    if (direction == Direction.right)
        return bitree_ins_right(tree, parent, data);

    return -1;
}

// Look up item assuming a binary tree organized as a binary search tree.
TreeNode* search_int(Tree* tree, int item)
{
    TreeNode* node = tree.root;

    while (node !is null)
    {
        if (item == *cast(int*)node.data)
        {
            return node;
        }
        else
        if (item < *cast(int*)node.data)
        {
            node = node.left;
        }
        else
        {
            node = node.right;
        }
    }

    return null;
}

unittest
{
    //~ test();
    //~ test_merge();
    test_traverse();
}

int* getInt(int i)
{
    auto res = new int;
    *res = i;
    return res;
}

void test_traverse()
{
    Tree tree;
    bitree_init(&tree, &free);

    bitree_ins_left(&tree, tree.root, getInt(1));
    bitree_ins_left(&tree, tree.root, getInt(2));
    bitree_ins_right(&tree, tree.root, getInt(3));
    bitree_ins_left(&tree, tree.root.left, getInt(4));
    bitree_ins_right(&tree, tree.root.left, getInt(5));

    writef("(Levelorder traversal)\n");
    print_levelorder(bitree_root(&tree));
}

void test_merge()
{
    Tree leftTree;
    bitree_init(&leftTree, &free);

    Tree rightTree;
    bitree_init(&rightTree, &free);

    insert_int(&leftTree, 10);
    insert_int(&leftTree, 20);
    insert_int(&rightTree, 30);
    insert_int(&rightTree, 40);

    Tree merged;
    int root = 100;
    bitree_merge(&merged, &leftTree, &rightTree, &root);
}

int test()
{
    Tree tree;
    TreeNode* node;

    int item;

    bitree_init(&tree, &free);

    writef("Inserting some nodes\n");

    if (insert_int(&tree, 20) != 0)
        return 1;

    if (insert_int(&tree, 10) != 0)
        return 1;

    if (insert_int(&tree, 30) != 0)
        return 1;

    if (insert_int(&tree, 15) != 0)
        return 1;

    if (insert_int(&tree, 25) != 0)
        return 1;

    if (insert_int(&tree, 70) != 0)
        return 1;

    if (insert_int(&tree, 80) != 0)
        return 1;

    if (insert_int(&tree, 23) != 0)
        return 1;

    if (insert_int(&tree, 26) != 0)
        return 1;

    if (insert_int(&tree, 5) != 0)
        return 1;

    writef("Tree size is %d\n", bitree_size(&tree));
    writef("(Preorder traversal)\n");
    print_preorder(bitree_root(&tree));

    item = 30;

    if ((node = search_int(&tree, item)) is null)
    {
        writef("Could not find %03d\n", item);
    }
    else
    {
        writef("Found %03d...Removing the left tree below it\n", item);
        bitree_rem_left(&tree, node);
        writef("Tree size is %d\n", bitree_size(&tree));
        writef("(Preorder traversal)\n");
        print_preorder(bitree_root(&tree));
    }

    item = 99;

    if ((node = search_int(&tree, item)) is null)
    {
        writef("Could not find %03d\n", item);
    }
    else
    {
        writef("Found %03d...Removing the right tree below it\n", item);
        bitree_rem_right(&tree, node);
        writef("Tree size is %d\n", bitree_size(&tree));
        writef("(Preorder traversal)\n");
        print_preorder(bitree_root(&tree));
    }

    item = 20;

    if ((node = search_int(&tree, item)) is null)
    {
        writef("Could not find %03d\n", item);
    }
    else
    {
        writef("Found %03d...Removing the right tree below it\n", item);
        bitree_rem_right(&tree, node);
        writef("Tree size is %d\n", bitree_size(&tree));
        writef("(Preorder traversal)\n");
        print_preorder(bitree_root(&tree));
    }

    item = bitree_is_leaf(bitree_root(&tree));
    writef("Testing bitree_is_leaf...Value=%d (0=OK)\n", item);
    item = bitree_is_leaf(bitree_left((bitree_root(&tree))));
    writef("Testing bitree_is_leaf...Value=%d (0=OK)\n", item);
    item = bitree_is_leaf(bitree_left(bitree_left((bitree_root(&tree)))));
    writef("Testing bitree_is_leaf...Value=%d (1=OK)\n", item);
    item = bitree_is_leaf(bitree_right(bitree_left((bitree_root(&tree)))));
    writef("Testing bitree_is_leaf...Value=%d (1=OK)\n", item);

    writef("Inserting some nodes\n");

    if (insert_int(&tree, 55) != 0)
        return 1;

    if (insert_int(&tree, 44) != 0)
        return 1;

    if (insert_int(&tree, 77) != 0)
        return 1;

    if (insert_int(&tree, 11) != 0)
        return 1;

    writef("Tree size is %d\n", bitree_size(&tree));
    writef("(Preorder traversal)\n");
    print_preorder(bitree_root(&tree));
    writef("(Inorder traversal)\n");
    print_inorder(bitree_root(&tree));
    writef("(Postorder traversal)\n");
    print_postorder(bitree_root(&tree));
    writef("(Levelorder traversal)\n");
    print_levelorder(bitree_root(&tree));

    // Destroy the binary tree.
    writef("Destroying the tree\n");
    stdout.flush();
    bitree_destroy(&tree);

    return 0;
}

auto bitree_size(T)(T tree)
{
    return tree.size;
}

auto bitree_root(T)(T tree)
{
    return tree.root;
}

auto bitree_is_eob(T)(T node)
{
    return node is null;
}

auto bitree_is_leaf(T)(T node)
{
    return node.left is null && node.right is null;
}

auto bitree_data(T)(T node)
{
    return node.data;
}

auto bitree_left(T)(T node)
{
    return node.left;
}

auto bitree_right(T)(T node)
{
    return node.right;
}
