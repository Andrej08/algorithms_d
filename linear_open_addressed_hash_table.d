/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module linear_open_addressed_hash_table;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

import c_singly_linked_list;
import hashpjw;

// http://en.wikipedia.org/wiki/Open_addressing
// todo: this is still not implemented (need to translate the JS code)

alias HashFunc = int function(void* key);

struct HashTable
{
    void* vacated;  // sentinel

    HashFunc hash;
    MatchFunction match;
    DestroyFunction destroy;

    int size;  // elements currently occupied in the table

    int positions;  // number of all positions in the table
    void** table;
}

auto ohtbl_size(T)(T htbl) { return htbl.size; }

/*****************************************************************************
*                                                                            *
*  Reserve a sentinel memory address for vacated elements.                   *
*                                                                            *
*****************************************************************************/

char vacated;

/*****************************************************************************
*                                                                            *
*  ------------------------------ ohtbl_init ------------------------------  *
*                                                                            *
*****************************************************************************/

int ohtbl_init(HashTable* htbl, int positions, HashFunc hash
               MatchFunction match, DestroyFunction destroy)
{
    int i;

    /*****************************************************************************
    *                                                                            *
    *  Allocate space for the hash table.                                        *
    *                                                                            *
    *****************************************************************************/

    if ((htbl.table = cast(void**)malloc(positions * (void*).sizeof)) is null)
        return -1;

    /*****************************************************************************
    *                                                                            *
    *  Initialize each position.                                                 *
    *                                                                            *
    *****************************************************************************/

    htbl.positions = positions;

    for (i = 0; i < htbl.positions; i++)
        htbl.table[i] = null;

    /*****************************************************************************
    *                                                                            *
    *  Set the vacated member to the sentinel memory address reserved for this.  *
    *                                                                            *
    *****************************************************************************/

    htbl.vacated = &vacated;

    /*****************************************************************************
    *                                                                            *
    *  Encapsulate the functions.                                                *
    *                                                                            *
    *****************************************************************************/

    htbl.hash    = hash;
    htbl.match   = match;
    htbl.destroy = destroy;

    /*****************************************************************************
    *                                                                            *
    *  Initialize the number of elements in the table.                           *
    *                                                                            *
    *****************************************************************************/

    htbl.size = 0;

    return 0;
}

/*****************************************************************************
*                                                                            *
*  ---------------------------- ohtbl_destroy -----------------------------  *
*                                                                            *
*****************************************************************************/

void ohtbl_destroy(HashTable* htbl)
{
    int i;

    if (htbl.destroy !is null)
    {
        /**************************************************************************
        *                                                                         *
        *  Call a user-defined function to free dynamically allocated data.       *
        *                                                                         *
        **************************************************************************/

        for (i = 0; i < htbl.positions; i++)
        {
            if (htbl.table[i] !is null && htbl.table[i] != htbl.vacated)
                htbl.destroy(htbl.table[i]);
        }
    }

    /*****************************************************************************
    *                                                                            *
    *  Free the storage allocated for the hash table.                            *
    *                                                                            *
    *****************************************************************************/

    free(htbl.table);

    /*****************************************************************************
    *                                                                            *
    *  No operations are allowed now, but clear the structure as a precaution.   *
    *                                                                            *
    *****************************************************************************/

    memset(htbl, 0, HashTable.sizeof);
}

// find first position with the data or an empty slot
private int find_slot(HashTable* htbl, void* data)
{
    auto position = htbl.hash(data) % htbl.positions;

    while (htbl.table[position] !is data && htbl.table[position] !is null)
        position = (position + 1) % htbl.positions;

    return position;
}

int ohtbl_lookup(HashTable* htbl, void* data)
{
    auto position = find_slot(htbl, data);
    return htable.table[position] is data ? 0 : -1;
}

int ohtbl_insert(HashTable* htbl, void* data)
{
    /*****************************************************************************
    *                                                                            *
    *  Do not exceed the number of positions in the table.                       *
    *                                                                            *
    *****************************************************************************/

    if (htbl.size == htbl.positions)
        return -1;

    /*****************************************************************************
    *                                                                            *
    *  Do nothing if the data is already in the table.                           *
    *                                                                            *
    *****************************************************************************/

    if (ohtbl_lookup(htbl, data) == 0)
        return 1;

    /*****************************************************************************
    *                                                                            *
    *  Use double hashing to hash the key.                                       *
    *                                                                            *
    *****************************************************************************/

    int position = htbl.hash(data) % htbl.positions;
    foreach (_; 0 .. htbl.positions)
    {
        if (htbl.table[position] is null || htbl.table[position] == htbl.vacated)
        {
            /***********************************************************************
            *                                                                      *
            *  Insert the data into the table.                                     *
            *                                                                      *
            ***********************************************************************/

            htbl.table[position] = cast(void*)data;
            htbl.size++;
            return 0;
        }

        // walk linearly
        position = (position + 1) % htbl.positions;
    }


    /*****************************************************************************
    *                                                                            *
    *  Return that the hash functions were selected incorrectly.                 *
    *                                                                            *
    *****************************************************************************/

    return -1;
}

int ohtbl_remove(HashTable* htbl, void** data)
{
    int position, i;

    /*****************************************************************************
    *                                                                            *
    *  Use double hashing to hash the key.                                       *
    *                                                                            *
    *****************************************************************************/

    for (i = 0; i < htbl.positions; i++)
    {
        position = (htbl.h1(*data) + (i * htbl.h2(*data))) % htbl.positions;

        if (htbl.table[position] is null)
        {
            /***********************************************************************
            *                                                                      *
            *  Return that the data was not found.                                 *
            *                                                                      *
            ***********************************************************************/

            return -1;
        }
        else if (htbl.table[position] == htbl.vacated)
        {
            /***********************************************************************
            *                                                                      *
            *  Search beyond vacated positions.                                    *
            *                                                                      *
            ***********************************************************************/

            continue;
        }
        else if (htbl.match(htbl.table[position], *data))
        {
            /***********************************************************************
            *                                                                      *
            *  Pass back the data from the table.                                  *
            *                                                                      *
            ***********************************************************************/

            *data = htbl.table[position];
            htbl.table[position] = htbl.vacated;
            htbl.size--;
            return 0;
        }
    }

    /*****************************************************************************
    *                                                                            *
    *  Return that the data was not found.                                       *
    *                                                                            *
    *****************************************************************************/

    return -1;
}

enum TBLSIZ = 11;

int match_char(void* char1, void* char2)
{
    return (*cast(char*)char1 == *cast(char*)char2);
}

int h1_char(void* key)
{
    return *cast(char*)key % TBLSIZ;
}

int h2_char(void* key)
{
    return 1 + (*cast(char*)key % (TBLSIZ - 2));
}

void print_table(HashTable* htbl)
{
    int i;

    writef("Table size is %d\n", ohtbl_size(htbl));

    for (i = 0; i < TBLSIZ; i++)
    {
        if (htbl.table[i] !is null && htbl.table[i] != htbl.vacated)
        {
            writef("Slot[%03d]=%c\n", i, *cast(char*)htbl.table[i]);
        }
        else
        {
            writef("Slot[%03d]= \n", i);
        }
    }

    return;
}

unittest
{
    test();
}

int test()
{
    HashTable htbl;

    char* data;
    char c;

    int retval, i, j;

    if (ohtbl_init(&htbl, TBLSIZ, &h1_char, &h2_char, &match_char, &free) != 0)
        return 1;

    /*****************************************************************************
    *                                                                            *
    *  Perform some open-addressed hash table operations.                        *
    *                                                                            *
    *****************************************************************************/

    for (i = 0; i < 5; i++)
    {
        writeln("Avoiding duplicates");

        if ((data = cast(char*)malloc(char.sizeof)) is null)
            return 1;

        /**************************************************************************
        *                                                                         *
        *  The following expression produces "random" data while avoiding dupli-  *
        *  cates.                                                                 *
        *                                                                         *
        **************************************************************************/

        *data = ((8 + (i * 9)) % 23) + 'A';

        writef("Hashing %c:", *data);

        for (j = 0; j < TBLSIZ; j++)
            writef(" %02d", (h1_char(data) + (j * h2_char(data))) % TBLSIZ);

        writef("\n");

        if (ohtbl_insert(&htbl, data) != 0)
            return 1;

        print_table(&htbl);
    }

    for (i = 0; i < 5; i++)
    {
        writeln("Not avoiding duplicates");

        if ((data = cast(char*)malloc(char.sizeof)) is null)
            return 1;

        /**************************************************************************
        *                                                                         *
        *  The following expression works similar to the one above but produces   *
        *  collisions.                                                            *
        *                                                                         *
        **************************************************************************/

        *data = ((8 + (i * 9)) % 13) + 'j';

        writef("Hashing %c:", *data);

        for (j = 0; j < TBLSIZ; j++)
            writef(" %02d", (h1_char(data) + (j * h2_char(data))) % TBLSIZ);

        writef("\n");

        if (ohtbl_insert(&htbl, data) != 0)
            return 1;

        print_table(&htbl);
    }

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'R';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Trying to insert R again...Value=%d (1=OK)\n", retval);

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'n';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Trying to insert n again...Value=%d (1=OK)\n", retval);

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'o';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Trying to insert o again...Value=%d (1=OK)\n", retval);

    writef("Removing R, n, and o\n");

    c    = 'R';
    data = &c;

    if (ohtbl_remove(&htbl, cast(void**)&data) == 0)
        free(data);

    c    = 'n';
    data = &c;

    if (ohtbl_remove(&htbl, cast(void**)&data) == 0)
        free(data);

    c    = 'o';
    data = &c;

    if (ohtbl_remove(&htbl, cast(void**)&data) == 0)
        free(data);

    print_table(&htbl);

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'R';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Hashing %c:", *data);

    for (j = 0; j < TBLSIZ; j++)
        writef(" %02d", (h1_char(data) + (j * h2_char(data))) % TBLSIZ);

    writef("\n");

    writef("Trying to insert R again...Value=%d (0=OK)\n", retval);

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'n';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Hashing %c:", *data);

    for (j = 0; j < TBLSIZ; j++)
        writef(" %02d", (h1_char(data) + (j * h2_char(data))) % TBLSIZ);

    writef("\n");

    writef("Trying to insert n again...Value=%d (0=OK)\n", retval);

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'o';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Hashing %c:", *data);

    for (j = 0; j < TBLSIZ; j++)
        writef(" %02d", (h1_char(data) + (j * h2_char(data))) % TBLSIZ);

    writef("\n");

    writef("Trying to insert o again...Value=%d (0=OK)\n", retval);

    print_table(&htbl);

    writef("Inserting X\n");

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'X';

    if (ohtbl_insert(&htbl, data) != 0)
        return 1;

    print_table(&htbl);

    if ((data = cast(char*)malloc(char.sizeof)) is null)
        return 1;

    *data = 'Y';

    if ((retval = ohtbl_insert(&htbl, data)) != 0)
        free(data);

    writef("Trying to insert into a full table...Value=%d (-1=OK)\n",
            retval);

    c    = 'o';
    data = &c;

    if (ohtbl_lookup(&htbl, data) == 0)
        writef("Found an occurrence of o\n");
    else
        writef("Did not find an occurrence of X\n");

    c    = 'Z';
    data = &c;

    if (ohtbl_lookup(&htbl, data) == 0)
        writef("Found an occurrence of Z\n");
    else
        writef("Did not find an occurrence of Z\n");

    /*****************************************************************************
    *                                                                            *
    *  Destroy the open-addressed hash table.                                    *
    *                                                                            *
    *****************************************************************************/

    writef("Destroying the hash table\n");
    ohtbl_destroy(&htbl);
    return 0;
}
