module test;

import core.stdc.stdlib;
import core.stdc.string;

import std.stdio;

/// Insertion sort
int issort(void* data, size_t size, size_t esize,
           int function(const void* key1, const void* key2) compare)
{
    char* a = cast(char*)data;

    // Allocate storage for the key element.
    void* key;
    if ((key = cast(char*)malloc(esize)) is null)
        return -1;

    // Repeatedly insert a key element among the sorted elements.
    foreach (j; 1 .. size)
    {
        memcpy(key, &a[j * esize], esize);
        size_t i = j - 1;

        // Determine the position at which to insert the key element.
        while (i >= 0 && compare(&a[i * esize], key) > 0)
        {
            memcpy(&a[(i + 1) * esize], &a[i * esize], esize);
            i--;
        }

        memcpy(&a[(i + 1) * esize], key, esize);
    }

    // Free the storage allocated for sorting.
    free(key);

    return 0;
}

void main()
{
}

