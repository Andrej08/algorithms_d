/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module shared_list;

import core.atomic;

shared struct SharedList(T)
{
    shared struct Node
    {
        private T _payload;
        private Node* _next;

        @property shared(Node)* next()
        {
            return clearlsb(_next);
        }

        bool removeAfter()
        {
            shared(Node)* thisNext;
            shared(Node)* afterNext;

            // step 1: set the lsb of _next for the node to delete
            do
            {
                thisNext = next;
                if (!thisNext)
                    return false;

                afterNext = thisNext.next;
            } while (!cas(&thisNext._next, afterNext, setlsb(afterNext)));

            // step 2: excise the node to delete
            if (!cas(&_next, thisNext, afterNext))
            {
                afterNext = thisNext._next;

                while (!haslsb(afterNext))
                    thisNext._next = thisNext._next.next;

                _next = afterNext;
            }

            return true;
        }

        void insertAfter(T value)
        {
            auto newNode = new shared Node(value);

            while (1)
            {
                // attempt to find an insertion point
                auto n = _next;
                while (n && haslsb(n))
                    n = n._next;

                // found a possible insertion point, attempt insert
                auto afterN = n._next;
                newNode._next = afterN;

                if (cas(&n._next, afterN, newNode))
                    break;
            }
        }
    }

    private Node* _root;

    void pushFront(T value)
    {
        auto n = new shared Node(value);
        shared(Node)* oldRoot;

        do
        {
            oldRoot = _root;
            n._next = oldRoot;
        } while (!cas(&_root, oldRoot, n));
    }

    shared(T)* popFront()
    {
        typeof(return) result;
        shared(Node)* oldRoot;

        do
        {
            oldRoot = _root;
            if (!oldRoot)
                return null;

            result = &oldRoot._payload;
        } while (!cas(&_root, oldRoot, oldRoot._next));

        return result;
    }
}

unittest
{
    auto list = new SharedList!int();
}

static bool haslsb(T)(T* p)
{
    return (cast(size_t)p & 1) != 0;
}

static T* setlsb(T)(T* p)
{
    return cast(T*)(cast(size_t)p | 1);
}

static T* clearlsb(T)(T* p)
{
    return cast(T*)(cast(size_t)p & ~1);
}
