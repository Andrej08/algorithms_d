/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module c_circular_singly_linked_list;

/**
    A straight port of the C circular singly-linked list structure to D.
*/

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;

/**
    Allocate a structure instance of type via C malloc,
    initialize it to T.init and return it.
*/
T* alloc(T)()
{
    auto result = enforce(cast(T*)malloc(T.sizeof));
    // note: malloc gives us untyped random memory, we have to initialize the memory.
    *result = T.init;
    return result;
}

/// An element of a singly-linked list.
struct ListElement
{
    void* data;
    ListElement* next;
}

/// The type of the function that will destroy data after a linked-list element is removed.
alias DestroyFunction = void function(void* data);

/// The type of the function for matching each element's data.
alias MatchFunction = int function(const(void)* data1, const(void)* data2);

/// The singly-linked list.
struct LinkedList
{
    /// The head of the list, null if list is empty.
    ListElement* head;

    /// The tail of the list, null if list is empty.
    ListElement* tail;

    /// The function that will destroy data after a linked-list element is removed.
    DestroyFunction destroy;

    /// The function for matching each element's data (unused in this module).
    MatchFunction match;

    /// The size of the list.
    size_t size;
}

/** Initialize the linked-list. $(D list) is assumed to point to valid memory. */
void list_init(LinkedList* list, DestroyFunction destroy = null)
{
    list.head = null;
    list.tail = null;
    list.size = 0;
    list.destroy = destroy;
}

/**
    Destroy the linked-list. If a non-null $(D destroy) function was provided in the
    call to $(D list_init), it will be invoked on each element of the linked-list
    after the element is removed from the list.
*/
void list_destroy(LinkedList* list)
{
    while (list.size > 0)
    {
        // remove the element from the head, but store back the data for removal.
        void* removed_data;
        list_rem_next(list, list.head, &removed_data);

        // call the cleanup destroy function for the removed data.
        if (list.destroy !is null)
            list.destroy(removed_data);
    }

    // clear the structure
    memset(list, 0, LinkedList.sizeof);
}

/**
    Insert $(D data) into the linked list after $(D old_element).
    If list is empty add $(D data) to the head of the list.

    $(B Note:) Adding to the head of the list is only supported
    when the list is empty.
*/
ListElement* list_ins_next(LinkedList* list, ListElement* old_element, void* data)
{
    // allocate the new old_element first and set its data pointer.
    auto new_element = alloc!ListElement();
    new_element.data = data;

    if (list_size(*list) == 0)  // add as head (will point to itself)
    {
        assert(old_element is null);  // sanity check
        new_element.next = new_element;
        list.head        = new_element;
    }
    else  // Handle insertion when the list is not empty.
    {
        new_element.next = old_element.next;
        old_element.next = new_element;
    }

    // update the size
    list.size++;

    return new_element;
}

/**
    Remove the element that follows $(D old_element) in the linked-list.
    $(D data) is a pointer to a pointer to data which will hold the
    data pointer of the removed element upon the return of the function.
*/
void list_rem_next(LinkedList* list, ListElement* old_element, void** data)
{
    enforce(list.size != 0);  // the list must not be empty.

    ListElement* removed_element;  // store element for freeing.

    if (old_element.next is old_element)
    {
        // Handle removing the last element.
        removed_element = old_element;
        list.head = null;
    }
    else
    {
        removed_element = old_element.next;
        old_element.next = old_element.next.next;
    }

    // release resources
    free(removed_element);

    // update the list size
    list.size--;
}

///
size_t list_size(LinkedList list) { return list.size; }

///
ListElement* list_head(LinkedList list) { return list.head; }

///
ListElement* list_tail(LinkedList list) { return list.tail; }

///
bool list_is_head(LinkedList list, ListElement* element) { return list.head is element; }

///
bool list_is_tail(LinkedList list, ListElement* element) { return list.tail is element; }

///
void* list_data(ListElement* element) { return element.data; }

///
ListElement* list_next(ListElement* element) { return element.next; }

///
unittest
{
    auto list = alloc!LinkedList();
    list_init(list);
    assert(list.size == 0);

    char* x = new char;
    *x = 'x';

    // add x to the head of the list (x)
    list_ins_next(list, null, x);
    assert(list.size == 1);

    char* y = new char;
    *y = 'y';

    // add y after x
    list_ins_next(list, list.head, y);
    assert(list.size == 2);

    char* z = new char;
    *z = 'z';

    // add z after head (x, z, y)
    list_ins_next(list, list.head, z);
    assert(list.size == 3);

    size_t idx;
    auto node = list.head;
    while (node !is null)
    {
        switch (idx)
        {
            case 0: assert(node.data is x); break;
            case 1: assert(node.data is z); break;
            case 2: assert(node.data is y); break;
            default: assert(0);
        }

        node = node.next;
        idx++;
        if (idx == 3)
            break;
    }

    list_destroy(list);
    assert(list.size == 0);
}
