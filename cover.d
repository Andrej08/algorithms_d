/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module cover;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

import c_singly_linked_list;
import set;

void print_skills(Set* skills)
{
    ListElement* member;
    char* data;
    int size, i;

    writef("Set size is %d\n", size = set_size(skills));

    i      = 0;
    member = list_head(skills);

    while (i < size)
    {
        data = cast(char*)list_data(member);
        writef("skills[%03d]=%c\n", i, *data);
        member = list_next(member);
        i++;
    }

    return;
}

void print_players(Set* players)
{
    ListElement* member;

    for (member = list_head(players); member !is null; member = list_next(member))
    {
        writef("Player %c\n", *cast(char*)(cast(KSet*)list_data(member)).key);
        print_skills(&(cast(KSet*)list_data(member)).set);
    }

    return;
}

int match_key(const(void)* key1, const(void)* key2)
{
    if (*cast(char*)(cast(const KSet*)key1).key == *cast(char*)(cast(const KSet*)key2).key)
        return 1;
    else
        return 0;
}

int match_skill(const(void)* skill1, const(void)* skill2)
{
    if (*cast(const char*)skill1 == *cast(const char*)skill2)
        return 1;
    else
        return 0;
}

struct KSet
{
    void* key;
    Set set;
}

enum SKILLS_COUNT = 12;
enum PLAYER_COUNT = 8;

unittest
{
    Set skills, players;
    char[SKILLS_COUNT] skills_array;
    char[PLAYER_COUNT] subids_array;
    KSet[PLAYER_COUNT] player_array;
    int retval, i;

    writef("Creating the set of skills\n");

    set_init(&skills, &match_skill, null);

    skills_array[0]  = 'a';
    skills_array[1]  = 'b';
    skills_array[2]  = 'c';
    skills_array[3]  = 'd';
    skills_array[4]  = 'e';
    skills_array[5]  = 'f';
    skills_array[6]  = 'g';
    skills_array[7]  = 'h';
    skills_array[8]  = 'i';
    skills_array[9]  = 'j';
    skills_array[10] = 'k';
    skills_array[11] = 'l';

    for (i = 0; i < SKILLS_COUNT; i++)
    {
        set_insert(&skills, &skills_array[i]);
    }

    print_skills(&skills);

    writef("Creating the player subsets\n");

    set_init(&players, &match_key, null);

    subids_array[0] = '1';
    subids_array[1] = '2';
    subids_array[2] = '3';
    subids_array[3] = '4';
    subids_array[4] = '5';
    subids_array[5] = '6';
    subids_array[6] = '7';
    subids_array[7] = '8';

    for (i = 0; i < PLAYER_COUNT; i++)
    {
        player_array[i].key = &subids_array[i];
        set_init(&player_array[i].set, &match_skill, null);

        switch (i)
        {
            case 0:
                set_insert(&player_array[i].set, &skills_array[0]);
                set_insert(&player_array[i].set, &skills_array[1]);
                set_insert(&player_array[i].set, &skills_array[2]);
                set_insert(&player_array[i].set, &skills_array[3]);
                break;

            case 1:
                set_insert(&player_array[i].set, &skills_array[4]);
                set_insert(&player_array[i].set, &skills_array[5]);
                set_insert(&player_array[i].set, &skills_array[6]);
                set_insert(&player_array[i].set, &skills_array[7]);
                set_insert(&player_array[i].set, &skills_array[8]);
                break;

            case 2:
                set_insert(&player_array[i].set, &skills_array[9]);
                set_insert(&player_array[i].set, &skills_array[10]);
                set_insert(&player_array[i].set, &skills_array[11]);
                break;

            case 3:
                set_insert(&player_array[i].set, &skills_array[0]);
                set_insert(&player_array[i].set, &skills_array[4]);
                break;

            case 4:
                set_insert(&player_array[i].set, &skills_array[1]);
                set_insert(&player_array[i].set, &skills_array[5]);
                set_insert(&player_array[i].set, &skills_array[6]);
                break;

            case 5:
                set_insert(&player_array[i].set, &skills_array[2]);
                set_insert(&player_array[i].set, &skills_array[3]);
                set_insert(&player_array[i].set, &skills_array[6]);
                set_insert(&player_array[i].set, &skills_array[7]);
                set_insert(&player_array[i].set, &skills_array[10]);
                set_insert(&player_array[i].set, &skills_array[11]);
                break;

            default:
                set_insert(&player_array[i].set, &skills_array[11]);
        }

        set_insert(&players, &player_array[i]);

    }

    print_players(&players);

    writef("Generating the cover\n");

    Set covering;
    if ((retval = cover(&skills, &players, &covering)) != 0)
        assert(0);

    if (retval == 1)
        stderr.writefln("The set could not be covered");
    else
        print_players(&covering);

    writef("Destroying the sets\n");
    stdout.flush();

    for (i = 0; i < PLAYER_COUNT; i++)
        set_destroy(&player_array[i].set);

    set_destroy(&skills);
}

int cover(Set* members, Set* subsets, Set* covering)
{
    Set intersection;
    KSet* subset;
    ListElement* member, max_member;
    void* data;
    int max_size;

    set_init(covering, subsets.match, null);

    // Continue while there are noncovered members and candidate subsets.
    while (set_size(members) > 0 && set_size(subsets) > 0)
    {
        // Find the subset that covers the most members.
        max_size = 0;

        for (member = list_head(subsets); member !is null; member = list_next(member))
        {
            // intersect the expected members (skills) with the current player (subset's) skills
            set_intersection(&intersection, &(cast(KSet*)list_data(member)).set, members);

            if (set_size(&intersection) > max_size)
            {
                max_member = member;
                max_size   = set_size(&intersection);
            }

            set_destroy(&intersection);
        }

        // A covering is not possible if there was no intersection.
        if (max_size == 0)
            return 1;

        // Insert the selected subset into the covering.
        subset = cast(KSet*)list_data(max_member);
        if (set_insert(covering, subset) != 0)
            return -1;

        // Remove each covered member (skill) from the set of noncovered members (skills) by walking through the
        // subset (player's) skills
        for (member = list_head(&(cast(KSet*)list_data(max_member)).set); member != null; member = list_next(member))
        {
            data = list_data(member);

            if (set_remove(members, cast(void**)&data) == 0 && members.destroy !is null)
                members.destroy(data);
        }

        // Remove the subset from the set of candidate subsets.
        if (set_remove(subsets, cast(void**)&subset) != 0)
            return -1;
    }

    // No covering is possible if there are still noncovered members.
    if (set_size(members) > 0)
        return -1;

    return 0;
}
