/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module queue;

import core.stdc.stdlib;

import std.exception;
import std.stdio;
import std.typecons;

import c_singly_linked_list;

alias Queue = LinkedList;

alias queue_init = list_init;

alias queue_destroy = list_destroy;

void* queue_peek(Queue queue)
{
    return (queue.head is null ? null : queue.head.data);
}

alias queue_size = list_size;

void queue_enqueue(Queue* queue, void* data)
{
    // add at tail
    list_ins_next(queue, list_tail(*queue), data);
}

void queue_dequeue(Queue* queue, void** data)
{
    // remove from head
    list_rem_next(queue, null, data);
}

void print_queue(Queue* queue)
{
    int size = queue_size(*queue);
    writefln("Queue size is %d.", size);

    ListElement* element = list_head(*queue);
    int i;

    while (i < size)
    {
        int* data = cast(int*)list_data(element);
        writefln("queue[%03d] = %03d", i, *data);
        element = list_next(element);
        i++;
    }
}

void print_top(Queue* queue)
{
    auto data = cast(int*)queue_peek(*queue);
    if (data !is null)
        writefln("Peeking at the top element... Value = %03d", *data);
    else
        writeln("Peeking at the top element... Value = null");

    print_queue(queue);
}

unittest
{
    //~ test();
}

void test()
{
    Queue queue;
    queue_init(&queue, &free);

    writeln("Enqueing 10 elements");

    foreach (i; 0 .. 10)
    {
        int* data = cast(int*)enforce(malloc(int.sizeof));
        *data = i + 1;
        queue_enqueue(&queue, data);
    }

    print_queue(&queue);

    writeln("Dequeing 5 elements");

    foreach (i; 0 .. 5)
    {
        int* data;
        queue_dequeue(&queue, cast(void**)&data);
        free(data);
    }

    print_queue(&queue);

    writeln("Enqueing 100 and 200");

    auto data = cast(int*)enforce(malloc(int.sizeof));
    *data = 100;
    queue_enqueue(&queue, data);

    data = cast(int*)enforce(malloc(int.sizeof));
    *data = 200;
    queue_enqueue(&queue, data);

    print_queue(&queue);

    print_top(&queue);

    writeln("Dequeing all elements");

    while (queue_size(queue) > 0)
    {
        queue_dequeue(&queue, cast(void**)&data);
        free(data);
    }

    print_top(&queue);

    writeln("Destroying the queue");
    queue_destroy(&queue);
}
