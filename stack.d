/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module stack;

import core.stdc.stdlib;

import std.exception;
import std.stdio;
import std.typecons;

import c_singly_linked_list;

alias Stack = LinkedList;

alias stack_init = list_init;

alias stack_destroy = list_destroy;

void* stack_peek(Stack stack)
{
    return (stack.head is null ? null : stack.head.data);
}

alias stack_size = list_size;

void stack_push(Stack* stack, void* data)
{
    // add at head
    list_ins_next(stack, null, data);
}

void stack_pop(Stack* stack, void** data)
{
    // remove from head
    list_rem_next(stack, null, data);
}

void print_stack(Stack* stack)
{
    int size = stack_size(*stack);
    writefln("Stack size is %d.", size);

    ListElement* element = list_head(*stack);
    int i;

    while (i < size)
    {
        int* data = cast(int*)list_data(element);
        writefln("stack[%03d] = %03d", i, *data);
        element = list_next(element);
        i++;
    }
}

void print_top(Stack* stack)
{
    auto data = cast(int*)stack_peek(*stack);
    if (data !is null)
        writefln("Peeking at the top element... Value = %03d", *data);
    else
        writeln("Peeking at the top element... Value = null");

    print_stack(stack);
}

unittest
{
    Stack stack;
    stack_init(&stack, &free);

    writeln("Pushing 10 elements");

    foreach (i; 0 .. 10)
    {
        int* data = cast(int*)enforce(malloc(int.sizeof));
        *data = i + 1;
        stack_push(&stack, data);
    }

    print_stack(&stack);

    writeln("Popping 5 elements");

    foreach (i; 0 .. 5)
    {
        int* data;
        stack_pop(&stack, cast(void**)&data);
        free(data);
    }

    print_stack(&stack);

    writeln("Pushing 100 and 200");

    auto data = cast(int*)enforce(malloc(int.sizeof));
    *data = 100;
    stack_push(&stack, data);

    data = cast(int*)enforce(malloc(int.sizeof));
    *data = 200;
    stack_push(&stack, data);

    print_stack(&stack);

    print_top(&stack);

    writeln("Popping all elements");

    while (stack_size(stack) > 0)
    {
        stack_pop(&stack, cast(void**)&data);
        free(data);
    }

    print_top(&stack);

    writeln("Destroying the stack");
    stack_destroy(&stack);
}
