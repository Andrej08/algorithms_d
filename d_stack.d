/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module d_stack;

import core.stdc.stdlib;

import std.exception;
import std.stdio;
import std.typecons;

import d_singly_linked_list;

class Stack(T)
{
    this()
    {
        list = new typeof(list);
    }

    T* peek()
    {
        return list.head is null ? null : list.head.data;
    }

    void push(T* data)
    {
        // add at head
        list.insert_front(data);
    }

    void pop(T** data)
    {
        // remove from head
        list.remove_front(data);
    }

    LinkedList!T list;
}
