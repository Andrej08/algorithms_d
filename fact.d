module test;

import std.stdio;

// note: not tail-recursive, multiple stack frames required
int fact(int n)
{
    if (n < 0)
        return 0;
    else
    if (n == 0 || n == 1)
        return 1;
    else
        return n * fact(n - 1);
}

// tail-recursive. a -- accumulator
// The function 2 is tail recursive because the single recursive call
// to facttail is the last statement executed before returning from the call.
int fact_tail(int n, int a = 1)
{
    if (n < 0)
        return 0;
    else
    if (n == 0)
        return 1;
    else
    if (n == 1)
        return a;
    else
        return fact_tail(n - 1, n * a);
}

void main()
{
    foreach (i; -1 .. 5)
        writefln("fact(%s) = %s", i, fact(i));

    foreach (i; -1 .. 5)
        writefln("fact_tail(%s) = %s", i, fact_tail(i));
}

