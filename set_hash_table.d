/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module set;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

import c_singly_linked_list;
import chained_hash_table;

struct Set
{
    LinkedList list;
    alias list this;
}

alias set_destroy = list_destroy;

auto set_size(Set set) { return set.size; }

auto set_size(const(Set)* set) { return set.size; }

void set_init(Set* set, MatchFunction match, DestroyFunction destroy)
{
    list_init(set, destroy);
    set.match = match;
}

/** Returns 0 if successful, 1 if data is in the set. */
int set_insert(Set* set, void* data)
{
    // Do not allow the insertion of duplicates.
    if (set_is_member(set, data))
        return 1;

    // Insert the data.
    list_ins_next(set, list_tail(set), data);
    return 0;
}

/**
    Remove data from the set.
    Returns 0 if data was removed, or -1 if data was not found.
*/
int set_remove(Set* set, void** data)
{
    ListElement* member, walk;

    for (member = list_head(set); member !is null; member = list_next(member))
    {
        if (set.match(*data, list_data(member)))
            break;

        // previous member is kept track of to allow removal from singly-linked list.
        // if walk is null it means removal of element from head (list_rem_next API).
        walk = member;
    }

    // Return if the member was not found.
    if (member is null)
        return -1;

    // Remove the member.
    list_rem_next(set, walk, data);
    return 0;
}

/** Builds a set that is a union of set1 and set2. */
void set_union(Set* setu, Set* set1, Set* set2)
{
    ListElement* member;
    void* data;

    // Initialize the set for the union.
    set_init(setu, set1.match, null);

    // Insert the members of the first set.
    for (member = list_head(set1); member !is null; member = list_next(member))
    {
        data = list_data(member);
        list_ins_next(setu, list_tail(setu), data);
    }

    // Insert the members of the second set.
    for (member = list_head(set2); member !is null; member = list_next(member))
    {
        // Do not allow the insertion of duplicates.
        if (set_is_member(set1, list_data(member)))
            continue;

        data = list_data(member);
        list_ins_next(setu, list_tail(setu), data);
    }
}

void set_intersection(Set* seti, Set* set1, Set* set2)
{
    ListElement* member;
    void* data;

    // Initialize the set for the intersection.
    set_init(seti, set1.match, null);

    // Insert the members present only in both sets.
    for (member = list_head(set1); member !is null; member = list_next(member))
    {
        if (set_is_member(set2, list_data(member)))
        {
            data = list_data(member);
            list_ins_next(seti, list_tail(seti), data);
        }
    }
}

void set_difference(Set* setd, Set* set1, Set* set2)
{
    ListElement* member;
    void* data;

    // Initialize the set for the difference.
    set_init(setd, set1.match, null);

    // Insert the members from set1 not in set2.
    for (member = list_head(set1); member !is null; member = list_next(member))
    {
        if (!set_is_member(set2, list_data(member)))
        {
            data = list_data(member);
            list_ins_next(setd, list_tail(setd), data);
        }
    }
}

bool set_is_member(Set* set, void* data)
{
    ListElement* member;

    // Determine if the data is a member of the set.
    for (member = list_head(set); member !is null; member = list_next(member))
    {
        if (set.match(data, list_data(member)))
            return true;
    }

    return false;
}

bool set_is_subset(Set* set1, Set* set2)
{
    ListElement* member;

    // Do a quick test to rule out some cases.
    if (set_size(set1) > set_size(set2))
        return false;

    // Determine if set1 is a subset of set2.
    for (member = list_head(set1); member !is null; member = list_next(member))
    {
        if (!set_is_member(set2, list_data(member)))
            return false;
    }

    return true;
}

bool set_is_equal(Set* set1, Set* set2)
{
    // Do a quick test to rule out some cases.
    if (set_size(set1) != set_size(set2))
        return false;

    // Sets of the same size are equal if they are subsets.
    return set_is_subset(set1, set2);
}

void print_set(Set* set)
{
    ListElement* member;

    int* data;
    int size, i;

    writef("Set size is %d\n", size = set_size(set));

    i      = 0;
    member = list_head(set);

    while (i < size)
    {
        data = cast(int*)list_data(member);
        writef("set[%03d]=%03d\n", i, *data);
        member = list_next(member);
        i++;
    }

    return;
}

/*****************************************************************************
*                                                                            *
*  ------------------------------- match_int ------------------------------  *
*                                                                            *
*****************************************************************************/

int match_int(const(void)* key1, const(void)* key2)
{
    /*****************************************************************************
    *                                                                            *
    *  Determine whether two integers match.                                     *
    *                                                                            *
    *****************************************************************************/

    return !memcmp(key1, key2, int.sizeof);
}

//~ unittest
//~ {
    //~ Set set,
        //~ set1,
        //~ set2;

    //~ int* data;
    //~ int retval, i;

    //~ /*****************************************************************************
    //~ *                                                                            *
    //~ *  Initialize the set.                                                       *
    //~ *                                                                            *
    //~ *****************************************************************************/

    //~ set_init(&set, &match_int, &free);

    //~ /*****************************************************************************
    //~ *                                                                            *
    //~ *  Perform some set operations.                                              *
    //~ *                                                                            *
    //~ *****************************************************************************/

    //~ writef("Inserting 10 members\n");

    //~ for (i = 9; i >= 0; i--)
    //~ {
        //~ data = enforce(cast(int*)malloc(int.sizeof));
        //~ *data = i + 1;

        //~ if ((retval = set_insert(&set, data)) == 1)
            //~ free(data);
    //~ }

    //~ print_set(&set);

    //~ writef("Inserting the same members again\n");

    //~ for (i = 9; i >= 0; i--)
    //~ {
        //~ data = enforce(cast(int*)malloc(int.sizeof));
        //~ *data = i + 1;

        //~ if ((retval = set_insert(&set, data)) == 1)
            //~ free(data);
    //~ }

    //~ print_set(&set);

    //~ writef("Inserting 200 and 100\n");

    //~ data = enforce(cast(int*)malloc(int.sizeof));
    //~ *data = 200;

    //~ if ((retval = set_insert(&set, data)) == 1)
        //~ free(data);

    //~ data = enforce(cast(int*)malloc(int.sizeof));
    //~ *data = 100;

    //~ if ((retval = set_insert(&set, data)) == 1)
        //~ free(data);

    //~ print_set(&set);

    //~ writef("Removing 100, 5, and 10\n");

    //~ i    = 100;
    //~ data = &i;

    //~ if (set_remove(&set, cast(void**)&data) == 0)
        //~ free(data);

    //~ i    = 5;
    //~ data = &i;

    //~ if (set_remove(&set, cast(void**)&data) == 0)
        //~ free(data);

    //~ i    = 10;
    //~ data = &i;

    //~ if (set_remove(&set, cast(void**)&data) == 0)
        //~ free(data);

    //~ print_set(&set);

    //~ writef("Removing three members\n");

    //~ list_rem_next(&set, null, cast(void**)&data);
    //~ free(data);

    //~ list_rem_next(&set, null, cast(void**)&data);
    //~ free(data);

    //~ list_rem_next(&set, null, cast(void**)&data);
    //~ free(data);

    //~ print_set(&set);

    //~ writef("Removing all members\n");

    //~ while (set_size(&set) > 0)
    //~ {
        //~ list_rem_next(&set, null, cast(void**)&data);
        //~ free(data);
    //~ }

    //~ print_set(&set);

    //~ /*****************************************************************************
    //~ *                                                                            *
    //~ *  Initialize two sets.                                                      *
    //~ *                                                                            *
    //~ *****************************************************************************/

    //~ set_init(&set1, &match_int, &free);
    //~ set_init(&set2, &match_int, &free);

    //~ /*****************************************************************************
    //~ *                                                                            *
    //~ *  Perform some set operations.                                              *
    //~ *                                                                            *
    //~ *****************************************************************************/

    //~ for (i = 9; i >= 0; i--)
    //~ {
        //~ data = enforce(cast(int*)malloc(int.sizeof));
        //~ *data = i + 1;

        //~ if ((retval = set_insert(&set1, data)) == 1)
            //~ free(data);

        //~ if (i == 5 || i == 6 || i == 7)
        //~ {
            //~ data = enforce(cast(int*)malloc(int.sizeof));
            //~ *data = i * 10;

            //~ if ((retval = set_insert(&set2, data)) == 1)
                //~ free(data);
        //~ }
        //~ else if (i == 3 || i == 1)
        //~ {
            //~ data = enforce(cast(int*)malloc(int.sizeof));
            //~ *data = i;

            //~ if ((retval = set_insert(&set2, data)) == 1)
                //~ free(data);
        //~ }
    //~ }

    //~ writef("Set 1 for testing unions, intersections, and differences\n");
    //~ print_set(&set1);
    //~ writef("Set 2 for testing unions, intersections, and differences\n");
    //~ print_set(&set2);

    //~ writef("Determining the union of the two sets\n");

    //~ set_union(&set, &set1, &set2);
    //~ print_set(&set);

    //~ writef("Destroying the union\n");
    //~ set_destroy(&set);

    //~ writef("Determining the intersection of the two sets\n");

    //~ set_intersection(&set, &set1, &set2);
    //~ print_set(&set);

    //~ writef("Testing whether the intersection equals Set 1...Value=%d "
            //~ "(0=OK)\n", set_is_equal(&set, &set1));

    //~ writef("Testing whether Set 1 equals itself...Value=%d (1=OK)\n",
            //~ set_is_equal(&set1, &set1));

    //~ writef("Testing whether the intersection is a subset of Set 1..."
            //~ "Value=%d (1=OK)\n", set_is_subset(&set, &set1));

    //~ writef("Testing whether Set 2 is a subset of Set 1...Value=%d "
            //~ "(0=OK)\n", set_is_subset(&set2, &set1));

    //~ writef("Destroying the intersection\n");
    //~ set_destroy(&set);

    //~ writef("Determining the difference of the two sets\n");

    //~ set_difference(&set, &set1, &set2);
    //~ print_set(&set);

    //~ i = 3;
    //~ writef("Testing whether %03d is in difference...Value=%d (0=OK)\n", i,
            //~ set_is_member(&set, &i));

    //~ i = 5;
    //~ writef("Testing whether %03d is in difference...Value=%d (1=OK)\n", i,
            //~ set_is_member(&set, &i));

    //~ /*****************************************************************************
    //~ *                                                                            *
    //~ *  Destroy the sets.                                                         *
    //~ *                                                                            *
    //~ *****************************************************************************/

    //~ writef("Destroying the sets\n");
    //~ stdout.flush();

    //~ set_destroy(&set);
    //~ set_destroy(&set1);
    //~ set_destroy(&set2);
//~ }
