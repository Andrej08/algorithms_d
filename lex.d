/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module lex;

import core.stdc.stdlib;
import core.stdc.string;

import std.ascii;
import std.exception;
import std.stdio;
import std.typecons;

import chained_hash_table;

// Define next_token for demonstration purposes.
char* next_token(char* istream)
{
    return null;
}

// Define a symbol structure for demonstration purposes.
struct Symbol
{
    char* lexeme;
    Token token;
}

// Define the token types recognized by the lexical analyzer.
alias Token = int;
enum : Token { lexit, error, digit, other }

Token lex(char* istream, HashTable* symtbl)
{
    Token token;
    Symbol* symbol;
    int length, retval, i;

    // Allocate space for a symbol.
    symbol = enforce(cast(Symbol*)malloc(Symbol.sizeof));

    // Process the next token.
    if ((symbol.lexeme = next_token(istream)) is null)
    {
        // Return that there is no more input.
        free(symbol);
        return lexit;
    }
    else
    {
        // Determine the token type.
        symbol.token = digit;
        length       = strlen(symbol.lexeme);

        for (i = 0; i < length; i++)
        {
            if (!isDigit(symbol.lexeme[i]))
                symbol.token = other;
        }

        memcpy(&token, &symbol.token, Token.sizeof);

        // Insert the symbol into the symbol table.
        if ((retval = chtbl_insert(symtbl, symbol)) < 0)
        {
            free(symbol);
            return error;
        }
        else
        if (retval == 1)
        {
            // The symbol is already in the symbol table.
            free(symbol);
        }
    }

    // Return the token for the parser.
    return token;
}

