/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module chained_hash_table;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

import c_singly_linked_list;
import hashpjw;

// Define a structure for chained hash tables.
struct HashTable
{
    int function(void* key) hashFunc;
    MatchFunction match;
    DestroyFunction destroy;

    int size;

    int bucketCount;
    LinkedList* table;  // array (buckets)
}

auto chtbl_size(HashTable hash)
{
    return hash.size;
}

auto chtbl_size(HashTable* hash)
{
    return hash.size;
}

void chtbl_init(HashTable* hash, int bucketCount,
                int function(void* key) hashFunc,
                MatchFunction match,
                DestroyFunction destroy)
{
    // Allocate space for the hash table.
    hash.table = enforce(cast(LinkedList*)malloc(bucketCount * LinkedList.sizeof));

    // Initialize the bucketCount.
    hash.bucketCount = bucketCount;

    foreach (ref list; hash.table[0 .. hash.bucketCount])
        list_init(&list, destroy);

    // Encapsulate the functions.
    hash.hashFunc       = hashFunc;
    hash.match   = match;
    hash.destroy = destroy;

    // Initialize the number of elements in the table.
    hash.size = 0;
}

void chtbl_destroy(HashTable* hash)
{
    // Destroy each bucket.
    foreach (ref list; hash.table[0 .. hash.bucketCount])
        list_destroy(&list);

    // Free the storage allocated for the hash table.
    free(hash.table);

    // No operations are allowed now, but clear the structure as a precaution.
    memset(hash, 0, HashTable.sizeof);
}

int chtbl_insert(HashTable* hash, void* data)
{
    void* temp = cast(void*)data;

    // Do nothing if the data is already in the table.
    if (chtbl_lookup(hash, &temp) == 0)
        return 1;

    // Hash the key.
    int bucket = hash.hashFunc(data) % hash.bucketCount;

    // Insert the data into the bucket.
    list_ins_next(&hash.table[bucket], null, data);
    hash.size++;

    return 0;
}

int chtbl_remove(HashTable* hash, void** data)
{
    // Hash the key.
    int bucket = hash.hashFunc(*data) % hash.bucketCount;

    // Search for the data in the bucket.
    ListElement* element, prev;

    for (element = list_head(&hash.table[bucket]); element !is null; element = list_next(element))
    {
        if (hash.match(*data, list_data(element)))
        {
            // Remove the data from the bucket.
            list_rem_next(&hash.table[bucket], prev, data);
            hash.size--;
            return 0;
        }

        prev = element;
    }

    // Return that the data was not found.
    return -1;
}

int chtbl_lookup(HashTable* hash, void** data)
{
    ListElement* element;
    int bucket;

    // Hash the key.
    bucket = hash.hashFunc(*data) % hash.bucketCount;

    // Search for the data in the bucket.
    for (element = list_head(&hash.table[bucket]); element !is null; element = list_next(element))
    {
        if (hash.match(*data, list_data(element)))
        {
            // Pass back the data from the table.
            *data = list_data(element);
            return 0;
        }
    }

    // Return that the data was not found.
    return -1;
}

enum TBLSIZ = 11;

int match_char(void* char1, void* char2)
{
    return (*cast(char*)char1 == *cast(char*)char2);
}

int h_char(void* key)
{
    //~ return *cast(char*)key % TBLSIZ;
    return hashpjw_hash(*cast(char*)key) % TBLSIZ;
}

void print_table(HashTable* hash)
{
    ListElement* element;
    int i;

    writef("\nTable size is %d\n", chtbl_size(hash));

    for (i = 0; i < TBLSIZ; i++)
    {
        writef("Bucket[%03d]=", i);

        for (element = list_head(&hash.table[i]); element !is null; element = list_next(element))
        {
            writef("%c", *cast(char*)list_data(element));
        }

        writef("\n");
    }

    return;
}

version (none)
unittest
{
    HashTable hash;

    char* data;
    char c;
    int retval, i;

    chtbl_init(&hash, TBLSIZ, &h_char, &match_char, &free);

    // Perform some chained hash table operations.
    for (i = 0; i < TBLSIZ; i++)
    {
        data = enforce(cast(char*)malloc(char.sizeof));

        *data = ((5 + (i * 6)) % 23) + 'A';

        enforce(chtbl_insert(&hash, data) == 0);

        print_table(&hash);
    }

    for (i = 0; i < TBLSIZ; i++)
    {
        data = enforce(cast(char*)malloc(char.sizeof));

        *data = ((3 + (i * 4)) % 23) + 'a';

        enforce(chtbl_insert(&hash, data) == 0);

        print_table(&hash);
    }

    data = enforce(cast(char*)malloc(char.sizeof));

    *data = 'd';

    if ((retval = chtbl_insert(&hash, data)) != 0)
        free(data);

    writef("Trying to insert d again...Value=%d (1=OK)\n", retval);
    print_table(&hash);

    data = enforce(cast(char*)malloc(char.sizeof));

    *data = 'G';

    if ((retval = chtbl_insert(&hash, data)) != 0)
        free(data);

    writef("Trying to insert G again...Value=%d (1=OK)\n", retval);
    print_table(&hash);

    writef("Removing d, G, and S\n");

    c    = 'd';
    data = &c;

    if (chtbl_remove(&hash, cast(void**)&data) == 0)
        free(data);

    c    = 'G';
    data = &c;

    if (chtbl_remove(&hash, cast(void**)&data) == 0)
        free(data);

    c    = 'S';
    data = &c;

    if (chtbl_remove(&hash, cast(void**)&data) == 0)
        free(data);

    print_table(&hash);

    data = enforce(cast(char*)malloc(char.sizeof));

    *data = 'd';

    if ((retval = chtbl_insert(&hash, data)) != 0)
        free(data);

    writef("Trying to insert d again...Value=%d (0=OK)\n", retval);

    data = enforce(cast(char*)malloc(char.sizeof));

    *data = 'G';

    if ((retval = chtbl_insert(&hash, data)) != 0)
        free(data);

    writef("Trying to insert G again...Value=%d (0=OK)\n", retval);

    print_table(&hash);

    writef("Inserting X and Y\n");

    data = enforce(cast(char*)malloc(char.sizeof));

    *data = 'X';

    enforce(chtbl_insert(&hash, data) == 0);

    data = enforce(cast(char*)malloc(char.sizeof));

    *data = 'Y';

    enforce(chtbl_insert(&hash, data) == 0);

    print_table(&hash);

    c    = 'X';
    data = &c;

    if (chtbl_lookup(&hash, cast(void**)&data) == 0)
        writef("Found an occurrence of X\n");
    else
        writef("Did not find an occurrence of X\n");

    c    = 'Z';
    data = &c;

    if (chtbl_lookup(&hash, cast(void**)&data) == 0)
        writef("Found an occurrence of Z\n");
    else
        writef("Did not find an occurrence of Z\n");

    /*****************************************************************************
    *                                                                            *
    *  Destroy the chained hash table.                                           *
    *                                                                            *
    *****************************************************************************/

    writef("Destroying the hash table\n");
    stdout.flush();
    chtbl_destroy(&hash);
}
