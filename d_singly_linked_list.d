/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module d_singly_linked_list;

/** A native D version of the C singly-linked list structure. */

import core.stdc.stdlib;
import core.stdc.string;

import std.conv;
import std.exception;
import std.stdio;

/// The singly-linked list.
class LinkedList(Data)
{
    /// An element of a singly-linked list.
    static class Node
    {
        ///
        this() { }

        ///
        this(Data* data) { _data = data; }

        ///
        @property inout(Data)* data() inout { return _data; }

        ///
        @property inout(Node) next() inout { return _next; }

    private:
        Data* _data;
        Node _next;
    }

    /** Initialize the linked-list. */
    void initialize(DestroyFunction destroyFunc = null)
    {
        _head = null;
        _tail = null;
        _size = 0;
        _destroyFunc = destroyFunc;
    }

    /**
        Destroy the linked-list. If a non-null $(D destroyFunc) function was provided in the
        call to $(D initialize), it will be invoked on each element of the linked-list
        after the element is removed from the list.
    */
    void destroy_list()
    {
        Data* removed_data;  // used to store removed data

        while (_size > 0)
        {
            // remove the element from the head
            this.remove_front(&removed_data);

            // and call the cleanup destroy function for the data
            if (_destroyFunc !is null)
                _destroyFunc(removed_data);
        }

        // clear this structure
        this.tupleof = typeof(this).tupleof.init;
    }

    /**
        Added: Insert $(D data) to the head of the linked list.
    */
    void insert_front(Data* data)
    {
        // allocate the new element first and set its data pointer.
        auto new_element = new Node();
        new_element._data = data;

        if (_size == 0)  // the new element is also the tail
            _tail = new_element;

        new_element._next = _head;

        // update head to new element
        _head = new_element;

        // update the size
        _size++;
    }

    /**
        Added: Insert $(D data) to the back of the linked list.
    */
    void insert_back(Data* data)
    {
        // allocate the new element first and set its data pointer.
        // note: the memory is not zero-initialized!
        auto newElem = new Node();
        newElem._data = data;
        newElem._next = null;  // adding to back means this element is the last.

        // the new element is also the head and the tail
        if (_size == 0)
        {
            _head = newElem;
            _tail = newElem;
        }
        else
        {
            // the new element goes after the last tail
            _tail._next = newElem;

            // the new element is now the tail
            _tail = newElem;
        }

        // update the size
        _size++;
    }

    /**
        Insert $(D data) into the linked list after $(D element).
        $(D element) must not be null.
    */
    void insert_next(Node element, Data* data)
    {
        enforce(element !is null);

        // allocate the new element first and set its data pointer.
        auto newElem = new Node;
        newElem._data = data;

        // update the new tail if necessary
        if (element.next is null)  /* or: if (list.tail is element) { } */
            _tail = newElem;

        // the new element must point to whatever the previous element pointed to.
        newElem._next = element.next;

        // new element follows the old element.
        element._next = newElem;

        // update the size
        _size++;
    }

    /**
        Remove the head element in the linked-list. $(D data) is a
        pointer to a pointer to data which will hold the data pointer
        of the element after the return of the function.
    */
    void remove_front(Data** data)
    {
        enforce(_size != 0);  // the list must not be empty
        enforce(_head !is null);

        // store the old data
        *data = _head.data;

        // set the new head
        _head = _head.next;

        // check and update the tail
        if (_size == 1)  // the last item was removed (no more tail)
            _tail = null;

        // update the list size
        _size--;
    }

    /**
        Remove the element that follows $(D element) in the linked-list. $(D data) is a
        pointer to a pointer to data which will hold the data pointer of the element
        after the return of the function. $(D element) must not be null.
    */
    void remove_next(Node element, Data** data)
    {
        enforce(_size != 0);  // the list must not be empty
        enforce(element !is null);

        enforce(element.next !is null);  // there must be a next element

        // store the old data
        *data = element.next.data;

        // point to the element after the to-be-destroyed element
        element._next = element._next._next;

        // check and update the tail
        if (element.next is null)
            _tail = element;

        // update the list size
        _size--;
    }

    /**
        Check whether this list is cyclic using an O(n) algorithm,
        which returns true if walking the elements surpasses
        the current linked-list size. $(D n) in O(n) is the size
        of the linked-list.
    */
    bool isCyclic()
    {
        size_t count;
        auto node = _head;
        while (node !is null && ++count <= _size)
            node = node.next;

        return count > size;
    }

    /**
        Equivalent to $(D isCyclic), using Floyd's Cycle-Finding Algorithm.
        A simple and easy to understand description is provided here:
        http://codingfreak.blogspot.com/2012/09/detecting-loop-in-singly-linked-list_22.html
    */
    bool isCyclicFloyd()
    {
        if (_size == 0)
            return false;

        auto slow = _head;
        auto fast = _head;

        do
        {
            if (fast.next is null || fast.next.next is null)
                return false;

            fast = fast.next.next;
            slow = slow.next;
        } while (slow !is fast);

        return true;
    }

    void unCycle()
    {
        unCycleBrent();
        //~ unCycleFloyd();
    }

    private void unCycleBrent()
    {
        if (_size == 0)
            return;

        auto walk = _head;
        auto target = _head;

        size_t limit = 1;
        size_t cyclic_count;  // the number of nodes in the cyclic range.

        writefln("begins: walk: %s target: %s cyclic_count: %s limit: %s",
                 *walk.data, *target.data, cyclic_count, limit);

        while (walk !is null)
        {
            if (walk.next is null)
                return;

            walk = walk.next;
            ++cyclic_count;

            if (walk is target)
                break;

            writefln("        walk: %s target: %s cyclic_count: %s limit: %s",
                     *walk.data, *target.data, cyclic_count, limit);

            if (cyclic_count == limit)
            {
                target = walk;
                limit *= 2;
                cyclic_count = 0;

                writefln("switch: walk: %s target: %s cyclic_count: %s limit: %s",
                         *walk.data, *target.data, cyclic_count, limit);
            }

            if (walk.next is target)
            {
                writefln("found:  walk: %s target: %s cyclic_count: %s limit: %s",
                     *walk.next.data, *target.data, cyclic_count + 1, limit);
            }
        }

        writeln("Uncycling.");

        unCycleFastImpl(cyclic_count);
        //~ unCycleSlowImpl(walk, cyclic_count);
    }

    private void unCycleFloyd()
    {
        if (_size == 0)
            return;

        auto slow = _head;
        auto fast = _head;

        do
        {
            if (fast.next is null || fast.next.next is null)
                return;

            fast = fast.next.next;
            slow = slow.next;
        } while (slow !is fast);

        auto ptr1 = slow;
        auto ptr2 = slow;
        size_t cyclic_count = 1;

        while (ptr1.next !is ptr2)  // count the number of nodes in the loop.
        {
            ptr1 = ptr1.next;
            ++cyclic_count;
        }

        unCycleFastImpl(cyclic_count);
        //~ unCycleSlowImpl(slow, cyclic_count);
    }

    private void unCycleFastImpl(size_t cyclic_count)
    {
        auto node1 = _head;  // set one pointer to head.

        // set another pointer to cyclic_count nodes after the head.
        auto node2 = _head;

        foreach (_; 0 .. cyclic_count)
            node2 = node2.next;

        /*
            Move both pointers at the same rate,
            they will meet at loop starting node.
        */
        while (node2 !is node1)
        {
            node1 = node1.next;
            node2 = node2.next;
        }

        // get the pointer to the last node.
        auto lastNode = node1.next;
        while (lastNode.next !is node1)
            lastNode = lastNode.next;

        // break the cycle.
        lastNode._next = null;
    }

    private void unCycleSlowImpl(Node slow, size_t cyclic_count)
    {
        /*
            Walk from head to tail until we reach the
            first node that the cyclic range points to.
        */
        auto curr = _head;
        do
        {
            // check whether the current node is
            // part of the cyclic range.
            foreach (_; 0 .. cyclic_count)
            {
                // found the start of the cycle
                if (slow.next is curr)
                {
                    // un-cycle and mark that node as the tail.
                    slow._next = null;
                    _tail = slow;
                    return;
                }

                slow = slow.next;
            }

            // check the next node.
            curr = curr.next;
        } while (curr);

        assert(0);  // cycle should have been found.
    }

    /**
        Note: Not added because removing the back element is an O(n) operation
        because this is a singly-linked list, not a doubly-linked list. We have
        no easy way of getting the element before the tail element.
    */
    // void remove_back(Data** data);

    /** String representation. */
    void toString(scope void delegate(const(char)[]) sink)
    {
        auto node = _head;
        while (node !is null)
        {
            static if (is(Data == char))
            {
                if (node.data !is null)
                {
                    char[1] data;
                    data[0] = *node.data;
                    sink(data[]);
                }
            }
            else
            {
                sink(to!string(*node.data));
            }

            node = node.next;
            if (node !is null)
                sink(", ");
        }
    }

    /**
        Reverse the representation of this linked-list.
        This is an O(n) operation.
    */
    void reverse()
    {
        if (_size == 0)  // nothing to reverse
            return;

        _tail = _head;  // current head becomes the new tail.

        Node prev;
        Node curr = _head;

        while (curr)
        {
            auto next = curr._next;  // store next

            curr._next = prev;
            prev = curr;

            curr = next;  // load next
        }

        _head = prev;  // last element traversed is the new head.
    }

    ///
    @property Node head() { return _head; }

    ///
    @property Node tail() { return _tail; }

    ///
    @property size_t size() { return _size; }

    ///
    bool isHead(Node element) { return _head is element; }

    ///
    bool isTail(Node element) { return _tail is element; }

    /// The type of the function pointer that will destroy data after a linked-list element is removed.
    alias DestroyFunction = void function(Data* data);

private:

    /// The head of the list, null if list is empty.
    Node _head;

    /// The tail of the list, null if list is empty.
    Node _tail;

    /// The size of the list
    size_t _size;

    /// The function pointer that will destroy data after a linked-list element is removed.
    DestroyFunction _destroyFunc;
}

version (none)
unittest
{
    auto list = new LinkedList!char;

    alias Node = typeof(list).Node;

    auto var1 = '1';
    auto var2 = '2';
    auto var3 = '3';
    auto var4 = '4';
    auto var5 = '5';
    auto var6 = '6';
    auto var7 = '7';
    auto var8 = '8';
    auto var9 = '9';

    auto el1 = new Node(&var1);
    auto el2 = new Node(&var2);
    auto el3 = new Node(&var3);
    auto el4 = new Node(&var4);
    auto el5 = new Node(&var5);
    auto el6 = new Node(&var6);
    auto el7 = new Node(&var7);
    auto el8 = new Node(&var8);
    auto el9 = new Node(&var9);

    el1._next = el2;
    el2._next = el3;
    el3._next = el4;
    el4._next = el5;
    el5._next = el6;
    el6._next = el7;
    el7._next = el8;
    el8._next = el9;
    el9._next = null;

    list._head = el1;
    list._tail = el1;
    list._size = 9;

    el8._next = el6;
    list.unCycle();

    // detecting a cycle and counting the length of the cycle
    // [1] [2] [3] [4] [5] [6] [7] [8] [9]
    //                      ^-----------^
    // iterations:
    enum _ = q{
    begins: walk: 1 target: 1 cyclic_count: 0 limit: 1
            walk: 2 target: 1 cyclic_count: 1 limit: 1
    switch: walk: 2 target: 2 cyclic_count: 0 limit: 2
            walk: 3 target: 2 cyclic_count: 1 limit: 2
            walk: 4 target: 2 cyclic_count: 2 limit: 2
    switch: walk: 4 target: 4 cyclic_count: 0 limit: 4
            walk: 5 target: 4 cyclic_count: 1 limit: 4
            walk: 6 target: 4 cyclic_count: 2 limit: 4
            walk: 7 target: 4 cyclic_count: 3 limit: 4
            walk: 8 target: 4 cyclic_count: 4 limit: 4
    switch: walk: 8 target: 8 cyclic_count: 0 limit: 8
            walk: 6 target: 8 cyclic_count: 1 limit: 8
            walk: 7 target: 8 cyclic_count: 2 limit: 8
    found:  walk: 8 target: 8 cyclic_count: 3 limit: 8
    };

    // calculating the start of the cycled range.
    // [1] [2] [3] [4] [5] [6] [7] [8] [9]
    //                      ^-----------^
    // n2 == head + length of cycle (4)
    //
    // [1] [2] [3] [4] [5] [6] [7] [8] [9]
    //  ^               ^   ^-----------^
    // n1              n2
    //
    // Test another one:
    // [1] [2] [3] [4] [5] [6] [7] [8] [9]
    //  ^               ^---------------^
    // n1                   ^
    //                     n2
    //
    //~ el9._next = el6;
    //~ list.unCycle();
}

///
version (none)
unittest
{
    char a = 'a';
    char b = 'b';
    char c = 'c';
    char d = 'd';

    auto list = new LinkedList!char;
    list.initialize();
    assert(list.size == 0);

    // add b to the head of the list (b)
    list.insert_front(&b);
    assert(list.size == 1);

    // add c to the head of the list (c, b)
    list.insert_front(&c);
    assert(list.size == 2);

    // add d after head (c, d, b)
    list.insert_next(list.head, &d);
    assert(list.size == 3);

    list.insert_back(&a); // (c, d, b, a)

    size_t idx;
    auto node = list.head;
    while (node !is null)
    {
        switch (idx)
        {
            case 0: assert(*node.data == c); break;
            case 1: assert(*node.data == d); break;
            case 2: assert(*node.data == b); break;
            case 3: assert(*node.data == a); break;
            default: assert(0);
        }

        writefln("Node: %s", *node.data);
        node = node.next;
        idx++;
    }

    list.destroy_list();
    assert(list.size == 0);

    list.initialize();
    list.insert_back(&a);
    list.insert_back(&b);
    list.insert_back(&c);
    list.insert_back(&d);

    writeln(list);

    list.reverse();
    writeln(list);

    list.reverse();
    writeln(list);

    list.destroy_list();
    list.initialize();
    list.insert_back(&a);

    writeln(list);

    list.reverse();
    writeln(list);

    list.insert_back(&b);
    writeln(list);

    list.reverse();
    writeln(list);

    list.initialize();
    writeln(list);

    list.reverse();
    writeln(list);
}

version (none)
unittest
{
    auto list = new LinkedList!char;

    alias Node = typeof(list).Node;

    auto var1 = '1';
    auto var2 = '2';
    auto var3 = '3';
    auto var4 = '4';
    auto var5 = '5';

    auto el1 = new Node(&var1);
    auto el2 = new Node(&var2);
    auto el3 = new Node(&var3);
    auto el4 = new Node(&var4);
    auto el5 = new Node(&var5);

    el1._next = el2;
    el2._next = el3;
    el3._next = el4;
    el4._next = el5;
    el5._next = el3;  // cyclic loop

    list._head = el1;
    list._tail = el1;
    list._size = 5;

    assert(list.isCyclic);
    assert(list.isCyclicFloyd);

    el5._next = null;
    assert(!list.isCyclic);
    assert(!list.isCyclicFloyd);

    //~ writeln(list);

    //~ el5._next = el5;
    //~ assert(list.isCyclic);
    //~ assert(list.isCyclicFloyd);

    //~ list.unCycle();
    //~ assert(!list.isCyclic);
    //~ assert(!list.isCyclicFloyd);

    //~ writeln(list);

    //~ el5._next = el4;
    //~ assert(list.isCyclic);
    //~ assert(list.isCyclicFloyd);

    //~ list.unCycle();
    //~ assert(!list.isCyclic);
    //~ assert(!list.isCyclicFloyd);

    //~ writeln(list);

    //~ el5._next = el3;
    //~ assert(list.isCyclic);
    //~ assert(list.isCyclicFloyd);

    //~ list.unCycle();
    //~ assert(!list.isCyclic);
    //~ assert(!list.isCyclicFloyd);

    //~ writeln(list);

    //~ el5._next = el2;
    //~ assert(list.isCyclic);
    //~ assert(list.isCyclicFloyd);

    //~ list.unCycle();
    //~ assert(!list.isCyclic);
    //~ assert(!list.isCyclicFloyd);

    //~ writeln(list);

    //~ el5._next = el1;
    //~ assert(list.isCyclic);
    //~ assert(list.isCyclicFloyd);

    //~ list.unCycle();
    //~ assert(!list.isCyclic);
    //~ assert(!list.isCyclicFloyd);

    //~ writeln(list);

    //~ el1._next = el1;
    //~ assert(list.isCyclic);
    //~ assert(list.isCyclicFloyd);

    //~ list.unCycle();
    //~ assert(!list.isCyclic);
    //~ assert(!list.isCyclicFloyd);

    //~ el1._next = el2;

    //~ writeln(list);
}
