/*
 *             Copyright Andrej Mitrovic 2013.
 *  Distributed under the Boost Software License, Version 1.0.
 *     (See accompanying file LICENSE_1_0.txt or copy at
 *           http://www.boost.org/LICENSE_1_0.txt)
 */
module hashpjw;

import core.stdc.stdlib;
import core.stdc.string;

import std.exception;
import std.stdio;
import std.typecons;

/*****************************************************************************
*                                                                            *
*  Define a table size for demonstration purposes only.                      *
*                                                                            *
*****************************************************************************/

enum PRIME_TBLSIZ = 1699;

// note: do not mod or multiply this function here.
int hashpjw_hash(char key)
{
    char* ptr = &key;
    int val;

    // Hash the key by performing a number of bit operations on it.
    val = (val << 4) + (*ptr);

    if (auto tmp = (val & 0xF000_0000))
    {
        val = val ^ (tmp >> 24);
        val = val ^ tmp;
    }

    return val;
}

// note: do not mod or multiply this function here.
// note: void* should never be used here, we can't assume this is a null-terminated string,
// it could be a pointer to a single character.
int hashpjw_hash(const(void)* key)
{
    char* ptr = cast(char*)key;
    int val;

    // Hash the key by performing a number of bit operations on it.
    while (*ptr != '\0')
    {
        val = (val << 4) + (*ptr);

        if (auto tmp = (val & 0xf0000000))
        {
            val = val ^ (tmp >> 24);
            val = val ^ tmp;
        }

        ptr++;
    }

    return val;
}

unittest
{
    // todo: try to implement the multiplication method later, also see druntime.

    // map the key to the table via the division method
    // Note: In practice, replace PRIME_TBLSIZ with the actual table size.

    //~ writeln(hashpjw_hash("foobar".ptr, PRIME_TBLSIZ));
    //~ writeln(hashpjw_hash("hoobar".ptr, PRIME_TBLSIZ));
}
