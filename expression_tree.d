/*
 *            Copyright Andrej Mitrovic 2013.
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 */
module expression_tree;

import core.stdc.stdlib;
import core.stdc.string;

import std.ascii;
import std.conv;
import std.exception;
import std.range;
import std.stdio;
import std.string;
import std.typecons;

import d_stack;

int evalExp(string exp)
{
    auto stack = new Stack!string();

    while (!exp.empty)
    {
        exp = exp.stripLeft;

        if (exp.front == '(')
        {
            stack.push("(");
            exp.popFront();
        }
        else
        if (exp.front.isDigit)
        {
            //~ string num = parse!int(exp).to!string;
        }

        break;
    }

    return 0;
}

unittest
{
    enum expression = "44 * 10 * (20 + 16) * 15 * (20 - (10 * 1) + 30)";
    auto res = evalExp(expression);
    writefln("res: %s. expected: %s", res, mixin(expression));

    //~ writeln(tree.size);
    //~ writeln(tree.compare);
    //~ writeln(tree.destroy);
    //~ writeln(tree.root);
}
